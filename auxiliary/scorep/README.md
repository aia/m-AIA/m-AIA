Information on scorep filters
=============================

This folder contains files with filtering rules for scorep. Each file is
supposed to contain a *sensible* set of rules that carefully balance performance
and level of detail in the reporting. If in doubt, level of detail is the
priority, but of course users are encouraged to add their own filtering if
desired.
