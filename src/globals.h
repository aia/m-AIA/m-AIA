// Copyright (C) 2024 The m-AIA AUTHORS
//
// This file is part of m-AIA (https://git.rwth-aachen.de/aia/m-AIA/m-AIA)
//
// SPDX-License-Identifier: LGPL-3.0-only

#ifndef MAIA_GLOBALS_H_
#define MAIA_GLOBALS_H_
////////////////////////////////////////////////////////////////////////////////
/// \file \brief This file includes MAIA's utility functions.
////////////////////////////////////////////////////////////////////////////////
/// Includes:
#include "COMM/mpioverride.h"
#include "INCLUDE/maiaconstants.h"
#include "INCLUDE/maiamacro.h"
#include "INCLUDE/maiatypes.h"
#include "IO/context.h"
#include "IO/infoout.h"
#include "MEMORY/alloc.h"
#include "MEMORY/scratch.h"
#include "UTIL/debug.h"
#include "UTIL/functions.h"
#include "UTIL/timer.h"
#include "compiler_config.h"
#include "config.h"
#include "enums.h"
#include "globalvariables.h"
#include "property.h"
#include "variables.h"
//#include "profiling.h" // TODO labels:TIMERS split from UTIL/timer.h
////////////////////////////////////////////////////////////////////////////////
#endif
