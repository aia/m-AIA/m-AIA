// Copyright (C) 2024 The m-AIA AUTHORS
//
// This file is part of m-AIA (https://git.rwth-aachen.de/aia/m-AIA/m-AIA)
//
// SPDX-License-Identifier: LGPL-3.0-only

#ifndef MAIA_ENUMS_H
#define MAIA_ENUMS_H
////////////////////////////////////////////////////////////////////////////////
/// \file \brief This file defines the enums used in MAIA
////////////////////////////////////////////////////////////////////////////////

#include "INCLUDE/maiatypes.h"

/* Additional enums must also be inserted in the string2enum
   function!
*/
enum FileType { NETCDF, HDF, TOML, ASCII, BINARY, VTK, VTU, VTP };

enum MpiTags { MAIA_MPI_DEFAULT_TAG, MAIA_MPI_FV_TAG, LPT_MPI_PARTICLE_RESPAWN_TAG, MAIA_MPI_LB_TAG };

enum SolverType {
  MAIA_FINITE_VOLUME,
  MAIA_FV_APE,
  MAIA_FV_GEQU_PV,
  MAIA_FV_LEVELSET,
  MAIA_FV_MB,
  MAIA_FV_MB_NEW_RK,
  MAIA_LEVELSET,
  MAIA_LS_SOLVER,
  MAIA_LEVELSET_SOLVER,
  MAIA_LS_FV,
  MAIA_LS_COMBUSTION,
  MAIA_MULTI_LS,
  MAIA_LATTICE_BOLTZMANN,
  MAIA_FINITE_CELL,
  MAIA_DISCONTINUOUS_GALERKIN,
  MAIA_DG_FV,
  MAIA_DG_MULTISOLVER,
  MAIA_STRUCTURED,
  MAIA_UNIFIED,
  MAIA_PARTICLE,
  MAIA_ACOUSTIC_ANALOGY,
  MAIA_RIGID_BODIES,
  MAIA_POST_DATA
};

enum GridType { MAIA_GRID_CARTESIAN, MAIA_GRID_STRUCTURED, MAIA_GRID_NONE };

// pascalm<
enum RansMethod {
  NORANS, // dummy
  RANS_SA,
  RANS_SA_DV,
  RANS_FS,
  RANS_KOMEGA,
  RANS_SST,
  RANS_KEPSILON
};
//>pascalm

enum SolverMethod {
  MAIA_RUNGE_KUTTA,
  MAIA_RUNGE_KUTTA_STRUCTURED,
  MAIA_RUNGE_KUTTA_GEQU_PV,
  MAIA_RUNGE_KUTTA_LEVELSET,
  MAIA_SEMI_LAGRANGE_LEVELSET,
  MAIA_SEMI_LAGRANGE_LEVELSET_LB,
  MAIA_ANALYTIC_LEVELSET,
  MAIA_LEVELSET_SURFACE,
  MAIA_RUNGE_KUTTA_MB_SEMI_LAGRANGE_LEVELSET,
  MAIA_RUNGE_KUTTA_MB_LEVELSET,
  MAIA_MAC_CORMACK,
  MAIA_LATTICE_BGK,
  MAIA_LATTICE_BGK_INIT,
  MAIA_LATTICE_BGK_GUO_FORCING,
  MAIA_LATTICE_BGKC,
  MAIA_LATTICE_BGKI_SMAGORINSKY,
  MAIA_LATTICE_BGKI_SMAGORINSKY2,
  MAIA_LATTICE_BGKI_SMAGO_WALL,
  MAIA_LATTICE_BGKI_DYNAMIC_SMAGO,
  MAIA_LATTICE_RBGK_DYNAMIC_SMAGO,
  MAIA_LATTICE_BGKI_EULER_2D,
  MAIA_LATTICE_BGK_THERMAL,
  MAIA_LATTICE_BGK_INNERENERGY,
  MAIA_LATTICE_BGK_TOTALENERGY,
  MAIA_LATTICE_BGK_TRANSPORT,
  MAIA_LATTICE_BGK_THERMAL_TRANSPORT,
  MAIA_LATTICE_BGK_INNERENERGY_TRANSPORT,
  MAIA_LATTICE_BGK_TOTALENERGY_TRANSPORT,
  MAIA_LATTICE_RBGK,
  MAIA_LATTICE_RBGK_SMAGORINSKY,
  MAIA_LATTICE_MRT_SMAGORINSKY,
  MAIA_LATTICE_MRT,
  MAIA_LATTICE_MRT2,
  MAIA_LATTICE_CLB,
  MAIA_LATTICE_CLB_SMAGORINSKY,
  MAIA_LATTICE_CUMULANT,
  MAIA_LINEAR_ELASTIC,
  MAIA_NONLINEAR_BAR,
  MAIA_FWH_FREQUENCY,
  MAIA_FWH_TIME
};

enum SensorType {
  DERIVATIVE,
  ENTROPY_GRADIENT,
  ENTROPY_QUOTIENT,
  VORTICITY,
  INTERFACE,
  PARTICLE,
  SPECIES,
  PATCH,
  TOTALPRESSURE,
  DIVERGENCE,
  CUTOFF,
  MEANSTRESS,
  SMOOTH,
  BAND
};

enum PostProcessingOps {
  PP_SLICE_AVERAGE,
  PP_REDUCE_TO_LEVEL_PRE,
  PP_REDUCE_TO_LEVEL_POST,
  PP_REDUCE_TO_LEVEL_AVERAGES_PRE,
  PP_AVERAGE_PRE,
  PP_AVERAGE_POST,
  PP_AVERAGE_IN,
  PP_COMPUTE_DIVERGENCEVELOCITY_PRE,
  PP_COMPUTE_DIVERGENCEVELOCITY_IN,
  PP_COMPUTE_DIVERGENCEVELOCITY_POST,
  PP_SPATIAL_AVERAGE_PRE,
  PP_SPATIAL_AVERAGE_POST,
  PP_SPATIAL_AVERAGE_IN,
  PP_MOVING_AVERAGE_PRE,
  PP_MOVING_AVERAGE_POST,
  PP_MOVING_AVERAGE_IN,
  PP_PROBE_POINT_PRE,
  PP_PROBE_POINT_POST,
  PP_PROBE_POINT_IN,
  PP_PROBE_LINE_PRE,
  PP_PROBE_LINE_POST,
  PP_PROBE_LINE_IN,
  PP_PROBE_LINE_PERIODIC_IN,
  PP_PROBE_LINE_PERIODIC_POST,
  PP_PROBE_ARB_LINE_PRE,
  PP_PROBE_ARB_LINE_POST,
  PP_PROBE_ARB_LINE_IN,
  PP_PROBE_SLICE_PRE,
  PP_PROBE_SLICE_POST,
  PP_PROBE_SLICE_IN,
  PP_PROBE_ARB_SLICE_PRE,
  PP_PROBE_ARB_SLICE_POST,
  PP_PROBE_ARB_SLICE_IN,
  PP_WRITEPOINTS_IN,
  PP_AVERAGE_SLICE_PRE,
  PP_TAUW_PRE,
  PP_SUBTRACT_PERIODIC_FLUCTUATIONS,
  PP_SUBTRACT_MEAN,
  PP_LOAD_AVERAGED_SOLUTION_PRE,
  PP_COMPUTE_PRODUCTION_TERMS_PRE,
  PP_COMPUTE_DISSIPATION_TERMS_PRE,
  PP_WRITE_GRADIENTS,
  PP_DECOMPOSE_CF,
  PP_SPRAY_STATS,
  PP_PARTICLE_SOLUTION,
  PP_POINT_SAMPLING_IN,
  PP_SURFACE_SAMPLING_IN,
  PP_VOLUME_SAMPLING_IN,
  PP_PARTICLE_STATISTICS,
  PP_ISO_TURBULENCE_STATISTICS,
  PP_PL_ISO_TURBULENCE_STATISTICS
};

enum GOVEQS { NAVIER_STOKES, EULER };

enum testCaseName { TINA_TC, SUZI_TC, NOZZLE_TC, ELBOW_TC };

enum DiscretizationScheme { LOCD, HOCD, HOCD_LIMITED, HOCD_LIMITED_SLOPES, HOCD_LIMITED_SLOPES_MAN };

enum AdvectiveFluxScheme { AUSM, AUSMPLUS, SLAU };

enum ViscousFluxScheme { FIVE_POINT, FIVE_POINT_MULTISPECIES, THREE_POINT, FIVE_POINT_STABILIZED };

enum TransportModel { Multi, Mix };

enum LevelSetMethod { STANDARD, LOCAL, FAST };

enum LevelSetReinitMethod {
  CR1,
  CR2,
  HCR1,
  HCR2,
  HCR2_LIMITED,
  HCR2_FULLREINIT,
  CR2PLUS,
  SUS5CR,
  RSU,
  DL1,
  DL2,
  SUS_1,
  SUS_1PLUS,
  SUS_2,
  SUS_WENO5,
  SUS_WENO5PLUS,
  ELL,
  no
};

enum LevelSetDiscretizationScheme {
  US1,
  UC3,
  UC3_SB,
  UC5,
  UC5_SB,
  UC11,
  WENO5,
  WENO5_SB,
  BACKWARDS_PAR,
  ROTATING_LS,
  ROTATING_BNDRY,
  BACKWARDS_INCREMENT
};

enum LevelSetBndryCnd { SYMMETRIC, PERIODIC };

enum ReactionScheme { METHANE_1_STEP, METHANE_2_STEP, NONE };

enum LbInitializationType {
  LB_TURBULENT_CHANNEL_INIT,
  LB_TURBULENT_MIXING_INIT,
  LB_TURBULENT_MIXING_FILTER_INIT,
  LB_TURBULENCE_ISOTROPIC_INIT,
  LB_TURBULENT_BOUNDARY,
  LB_TURBULENT_PIPE_INIT,
  LB_TURBULENT_DUCT_INIT,
  LB_FROM_ZERO_INIT,
  LB_LAMINAR_INIT_PX,
  LB_LAMINAR_INIT_MX,
  LB_LAMINAR_INIT_PY,
  LB_LAMINAR_INIT_MY,
  LB_LAMINAR_INIT_PZ,
  LB_LAMINAR_INIT_MZ,
  LB_LAMINAR_CHANNEL_INIT,
  LB_LAMINAR_CYLINDER_INIT,
  LB_LAMINAR_PIPE_INIT,
  LB_VORTEX_INIT,
  LB_TGV_INIT,
  LB_GAUSS_PULSE_INIT,
  LB_GAUSS_DIFFUSION_INIT,
  LB_GAUSS_ADVECTION_INIT,
  LB_SPINNING_VORTICIES_INIT,
  LB_STEADY_VORTEX_INIT,
  LB_CONVECTING_VORTEX_INIT
};

enum LbInterfaceMethod { ROHDE, FILIPPOVA };

// Add enum for LB adaptation initialisation methos
enum LbAdaptationInitMethod { INIT_COPYPASTE, INIT_DUPUIS_FILIPPOVA };

enum LbNonNewtonianModel { POWERLAW, CARREAU };

enum LbInterpolationType { QUADRATIC_INTERPOLATION, LINEAR_INTERPOLATION, CUBIC_INTERPOLATION };

enum FcInterpolationType { EQUIDIST_LAGRANGE_INTERP, LAGRANGE_INTERP, EQUIDIST_LEGENDRE_INTERP, LEGENDRE_INTERP };

enum VariableType { MINT, MLONG, MFLOAT, MSTRING, MBOOL, MINVALID }; // DOUBLE;

enum LbModelType { D2Q9, D3Q15, D3Q19, D3Q27 };

enum PropertiesGroups {
  LEVELSET,
  LEVELSETMB,
  COMBUSTION,
  LS_RANS,
  LB,
  DG,
  LS_SOLVER,
  LEVELSET_LB,
  PROPERTIESGROUPS_COUNT
};

enum RealTimerGroup {
  TIME_TOTAL,
  TIME_INTEGRATION,
  TIME_RHS,
  TIME_RHS_BND,
  TIME_LHS,
  TIME_LHS_BND,
  TIME_EXCHANGE,
  TIME_ADAPTION,
  TIME_LS_PREPARE_SOLVER,
  TIME_LS_INTEGRATION,
  TIME_LS_FINALIZE,
  TIME_LS_REBUILD_TUBE,
  TIME_LS_REINITIALIZATION,
  TIME_PART_TOTAL,
  TIME_PART_MOVE,
  TIME_PART_TRANSFER,
  TIME_PART_DETECT_COLL,
  TIME_PART_RESPAWN,
  TIME_GROUP_COUNT
};

// Discontinuous Galerkin enums

// Available polynomials
enum DgPolynomialType { DG_POLY_LEGENDRE };

// Available integration methods
enum DgIntegrationMethod { DG_INTEGRATE_GAUSS, DG_INTEGRATE_GAUSS_LOBATTO };

// Available time integration schemes
// The naming scheme is "DG_TIMEINTEGRATION_<name>_<order>_<stages>" with the following components:
// - name: a descriptive name of the scheme, usually the main author(s) (e.g., Carpenter)
// - order: the formal order of accuracy (e.g., 4th-order accurate)
// - stages: the number of Runge-Kutta stages per time step (e.g., 14)
enum DgTimeIntegrationScheme {
  DG_TIMEINTEGRATION_CARPENTER_4_5,
  DG_TIMEINTEGRATION_TOULORGEC_4_8,
  DG_TIMEINTEGRATION_NIEGEMANN_4_14,
  DG_TIMEINTEGRATION_NIEGEMANN_4_13,
  DG_TIMEINTEGRATION_TOULORGEC_3_7,
  DG_TIMEINTEGRATION_TOULORGEF_4_8
};

// Available systems of equations
enum DgSystemEquations { DG_SYSEQN_EULER, DG_SYSEQN_LINEARSCALARADV, DG_SYSEQN_ACOUSTICPERTURB };

enum FvSystemEquations { FV_SYSEQN_RANS, FV_SYSEQN_EEGAS, FV_SYSEQN_NS, FV_SYSEQN_DETCHEM };

enum BcType { BC_DIRICHLET, BC_NEUMANN, BC_ROBIN, BC_ISOTHERMAL, BC_UNSET };

enum DgAdaptiveMethod { DG_ADAPTIVE_NONE, DG_ADAPTIVE_TEST, DG_ADAPTIVE_GRADIENT };

// Structured Solver enums

// Available structured solver limiters
enum limiterMethod { VENKATAKRISHNAN_MOD, VENKATAKRISHNAN, BARTH_JESPERSON, MINMOD, ALBADA, PTHRC, LEASTSQUARES };

enum StructuredCommType { PARTITION_NORMAL, PERIODIC_BC, PERIODIC_BC_SINGULAR, SINGULAR };

// Particle enums

// Available distribution functions
enum PartEmittDist { PART_EMITT_DIST_NONE, PART_EMITT_DIST_UNIFORM, PART_EMITT_DIST_GAUSSIAN };

// Available spray angle relationships
enum SprayAngleModel {
  SPRAY_ANGLE_MODEL_CONST,
  SPRAY_ANGLE_MODEL_HIROYASU_ARAI80,
  SPRAY_ANGLE_MODEL_HIROYASU_ARAI90,
  SPRAY_ANGLE_MODEL_BRACO_REITZ,
  SPRAY_ANGLE_MODEL_REITZ
};

enum Phase { GAS, LIQUID, SOLID };

enum InjectorType { FULLCONE, HOLLOWCONE, MULTIHOLE, MULTIHOLE_OPT, MULTIHOLE_TME };

// surface types enums

enum SolverSurfaceType { STL, ANALYTIC_BOX, ANALYTIC_SPHERE };

// Dynamic load balancing partitioning methods
enum DlbPartitionMethods {
  DLB_PARTITION_DEFAULT,
  DLB_PARTITION_WEIGHT,
  DLB_PARTITION_SHIFT_OFFSETS,
  DLB_PARTITION_TEST
};

// Acoustic extrapolation methods
enum ExtrapolationMethods {
  // Ffowcs Williams-Hawkings method
  FWH_METHOD,
  // Ffowcs Williams-Hawkings method based on perturbed acoustic perturbation equs. (APE) variables
  FWH_APE_METHOD
};

// Solver variable identifiers for sampling
enum FvCartesianSolverSamplingQuantities { FV_PV, FV_VORT, FV_HEAT_RELEASE };
enum DgCartesianSolverSamplingQuantities { DG_VARS, DG_NODEVARS, DG_SOURCETERMS };
enum LbSolverSamplingQuantities { LB_PV };

// Unified run loop - coupler types
enum CouplerType {
  COUPLER_LS_FV_MB,
  COUPLER_LS_FV,
  COUPLER_LS_FV_COMBUSTION,
  COUPLER_FV_MULTILEVEL,
  COUPLER_FV_MULTILEVEL_INTERPOLATION,
  COUPLER_FV_ZONAL_RTV,
  COUPLER_FV_ZONAL_STG,
  COUPLER_CARTESIAN_INTERPOLATION,
  COUPLER_FV_DG_APE,
  COUPLER_FV_PARTICLE,
  COUPLER_LS_LB,
  COUPLER_LS_LB_SURFACE,
  COUPLER_LS_LB_PARTICLE,
  COUPLER_FV_MB_ZONAL,
  COUPLER_LB_LPT,
  COUPLER_LB_FV_EE_MULTIPHASE,
  COUPLER_LB_LB,
  COUPLER_LB_DG_APE,
  COUPLER_LB_RB
};

// Unified run loop - postprocessing types
enum PostprocessingType {
  POSTPROCESSING_FV,
  POSTPROCESSING_LS,
  POSTPROCESSING_LB,
  POSTPROCESSING_DG,
  POSTPROCESSING_FVLPT,
  POSTPROCESSING_LBLPT
};

// Unified run loop - exuction recipes
enum Recipe { RECIPE_INTRASTEP, RECIPE_BASE, RECIPE_ITERATION };

enum LbRefillingSchemeMb { NORMAL_EXTRAPOLATION, AVERAGED_EXTRAPOLATION, EQ_NEQ, VEL_CONSTRAINED_NORMAL_EXTRAPOLATION };

enum LPTmpiTag { PARTICLE_COUNT, PARTICLE_FLOAT, PARTICLE_INT, SOURCE_TERMS, FLOW_FIELD, CHECK_ADAP, VELOCITY_SLOPES };

enum LbBounceBackSchemeMb { BOUZIDI_LINEAR, BOUZIDI_QUADRATIC, YU_LINEAR, YU_QUADRATIC };

enum ViscosityLaw { SUTHERLAND, CONSTANT };

MInt string2enum(MString);

#endif
