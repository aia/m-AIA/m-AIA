Overview: MAIA documentation for the DG block
============================================

The following documents are available:

*   **autosave.md**: documentation for the end autosave feature of the DG block
*   **firststeps.md**: a tutorial for the novice MAIA user with a focus on the DG
                       block
*   **hp_refinement.md**: information on the (adaptive) hp-refinement features
                          in the DG block, including some details on the
                          implementation
*   **point_data.md**: documentation for the point data feature of the DG block
*   **maximum_CFLs.pdf**: documentation for the maximum stable CFL number in the DG block
