# Numerical Methods and Algorithms # {#numericalMethods}

Enim cillum deserunt pariatur aute id ut incididunt velit ea occaecat. Nulla ullamco tempor esse aute commodo enim exercitation nisi commodo nostrud pariatur amet adipisicing qui. Deserunt adipisicing nostrud non ut commodo. Elit laboris eiusmod nisi consectetur culpa aliqua nisi. Laborum esse adipisicing fugiat magna duis deserunt dolore consequat mollit cupidatat eu ipsum. Laboris ipsum labore nisi anim.

@subpage nmCartesianGrid  
@subpage nmStructuredGrid  
@subpage nmSolvers  
@subpage nmCoupling  
@subpage nmParallelization  
@subpage nmAMR  
@subpage nmDLB  
