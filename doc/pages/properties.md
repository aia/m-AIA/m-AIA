# Properties Documentation Page #

A complete list of all properties:

@subpage propertiesGlobal "Global"<br>
@subpage propertiesCoupling "Solver Coupling"<br>

@subpage propertyPageACA "ACA"<br>
@subpage propertyPageDG "DG"<br>
@subpage propertyPageLPT "LPT"<br>
@subpage propertyPageMaterial "Material"<br>
@subpage propertiesLS "LS"<br>
@subpage propertiesFV "FV"<br>
@subpage propertiesLB "LB"<br>
@subpage propertiesFVMB "FVMB"<br>

@subpage propertiesDLB "DLB"<br>
@subpage propertiesAMR "AMR"<br>
@subpage propertiesHPC "HPC"<br>
@subpage propertiesPP "Post-Processing"<br>

@subpage propertiesFVSTRCTRD "FV STRUCTURED"<br>

@subpage propertyPage1
