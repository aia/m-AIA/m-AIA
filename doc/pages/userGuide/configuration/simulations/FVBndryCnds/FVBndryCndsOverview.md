# List of FV Boundary Conditions # {#fvBcOverview}
The Finite Volume solver has several categories of boundary conditions.
An overview can be found here:

* @subpage fvBc "Ghost Cell Boundary Conditions"
* @subpage fvCutoffBc "Cutofff Boundary Conditions"
* @subpage fvCbc "Characteristic Boundary Conditions"

