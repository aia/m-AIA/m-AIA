# List of LB boundary conditions # {#LBBCList}
@note Boundary conditions with the Id `MB` are set through moving-boundary properties, not a normal BC Id.