# Tutorials # {#ugTutorials}

MAIA is divided into two distinct parts, one uses a hierarchical Cartesian cut-cell approach with automatic grid generation techniques (**Cartesian grid**). The second part is the structured block environment for solving the Navier-Stokes equations (NSE) on curvilinear boundary fitted grids (**Structured grid**). This page provides an introduction into the use of the Cartesian and structured solver part of MAIA by using short and cheap-to-compute tutorials.  

## Cartesian grid

@subpage ugTutorial1cartesian  
@subpage ugTutorial2cartesian  
@subpage ugTutorial3cartesian  
@subpage ugTutorial4cartesian  
@subpage ugTutorial5cartesian  
@subpage ugTutorial6cartesian  
@subpage ugTutorial7cartesian  
Tutorial 8 (Post-Processing)

## Structured grid

@subpage ugTutorial1structured  
@subpage ugTutorial2structured  
@subpage ugTutorial3structured  
Structured Tutorial 4 (LES Initialization)    
Structured Tutorial 5 (LES Reference)  
Structured Tutorial 6 (LES Actuated)  
Structured Tutorial 7 (Post-Processing)  
