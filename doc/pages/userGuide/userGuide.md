# User Guide # {#userGuide}

[TOC]

This user guide is intended to assist users in applying @maia for the simulation of physical problems in the field of computational fluid dynamics, computational aeroacoustics, conjugate heat transfer, etc.. The intended audience are researchers such as PhD or Master students and engineers, who are familiar with the linux operating system.

The first step to be performed is to obtain the @maia source code from the git repository, install all necessary libraries and to compile the code, see section [Getting started](@ref ugGettingStarted). After successful installation and compilation, it is recommended to start with the [Tutorials](@ref ugTutorials), in which mesh generation, simulation and visualization are demonstrated for simple applications to go through the individual tasks necessary for a simulation. 
  
In case more details are required, please consult the [developer guide](@ref devGuide), which includes a lot of additional information.

@subpage ugGettingStarted  
@subpage ugGettingStartedUsefulKnowledge  
@subpage ugGettingStartedAIAUsers  
@subpage ugInstallationGuide  
@subpage ugTutorials  
@subpage ugSimulationWorkflow  
@subpage ugConfiguration  
@subpage ugParallelization  
@subpage ugVisualization  

