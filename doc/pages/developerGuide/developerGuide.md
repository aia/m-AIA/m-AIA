# Developer Guide # {#devGuide}

Here you will find useful information if you want to start contributing to this project. We will cover our workflow using GitLab, automatic and manual testing, essential concepts and building blocks, and documentation guidelines.

## Workflow

Our development workflow is centered around GitLab and its continuous integration (CI) features as well as our own testing environment canary.
We welcome everybody to contribute to this project by reporting existing problems or new ideas as __issues__, or propose changes via __merge requests (MR)__.
Read more about @subpage dvGitlab "GitLab".

During your development and especially before you want to bring your changes back to the master, make sure the code is still running as intended.
Read more about our @subpage dvTesting "testing" environment to learn how to check this.

Comprehensive documentation is essential for the conservation of know-how and saves a lot of time when it comes to the introduction of new @maia users/developers.
Writing documentation should be part of every development endeavour. The principles of this @subpage dvDocumentation "documentation" are explained and supplemented by some @subpage dvExamples "examples".

## Concepts

You can find more information about the fundamental "concepts and building blocks" of our solver framework on the following pages:

- @subpage dvSolver
- @subpage dvCoupler
- @subpage dvAMR
- @subpage dvDLB
- @subpage dvRecipe  
- @subpage dvDataStructures
- @subpage dvCollector
- @subpage dvProxy
