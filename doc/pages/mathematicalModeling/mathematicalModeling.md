# Mathematical Modeling # {#mathematicalModeling}

All numerical solvers in the software framework @maia are supposed to predict numerical solutions of partial differential equations, that represent a mathematical model for a specific physical problem. The corresponding formulations of these equations and related models can be found in the following subsections:

@subpage mmNS  
@subpage mmCombustion
@subpage mmLBE  
@subpage mmAPE  
@subpage mmFWH  
@subpage mmEnthalpy  
@subpage mmLSA  
@subpage mmLS  
@subpage mmLPT  
@subpage mmFC
