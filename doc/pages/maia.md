# MAIA # {#mainpage}


@maia (multi-physics AIA) is a multi-physics partial differential
equation (PDE) solver framework with a focus on problems related to
computational fluid dynamics, computational aeroacoustics and
structural mechanics. It is developed at the Institute of Aerodynamics
(AIA) of the RWTH Aachen University in Germany. The source code and
documentation can be found on the @maia [gitlab
page](https://git.rwth-aachen.de/aia/MAIA/Solver).

This documentation consists of the following sections:

* @subpage userGuide
This section contains everything you need to know
to install @maia and perform a simulation. It also includes the most
important settings required for a specific simulation.

* @subpage mathematicalModeling  
The mathematical models, i.e., the governing equations with additional models for physical processes such as turbulence, sprays, combustion, etc. are briefly
summarized in this section. References to papers are included, which may contain additional details.

* @subpage numericalMethods  
In this section, the numerical methods used for the solution of the partial differential equations are described. It includes details of the mesh generation, time integration,  spatial discretization, parallelization algorithms.

* @subpage devGuide  
The developers guide include information about the source code with its data structures and the workflow for the introduction of new functionality. It also includes guidelines for the documentation, code implementation and regression checks.
