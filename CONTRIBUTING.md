# Contributing

This code is a joint endeavour of many people. We are happy to accept contributions from the community any time.
Feel free to report issues or submit changes as merge requests.
For larger contributions, it's always a good idea to open an issue first to organize the effort and avoid duplicate work.

If you want to learn more about our development workflow, including testing and documentation, please refer to the developer guide in our documentation.
